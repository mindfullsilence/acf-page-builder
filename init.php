<?php
add_action('after_setup_theme', function() {
    if(!is_array(Timber::$locations)) {
        Timber::$locations = array(Timber::$locations);
    }
    Timber::$locations[] = dirname(__FILE__);
});
